﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF12_2018_POP2021.Servisi
{
    public interface IServis<T, R>
    {
        R DodajNovi(T obj);

        ObservableCollection<T> Citaj();

        T NadjiPoId(R id);

        R Azuriraj(T obj);

        R Obrisi(T obj);
    }
}

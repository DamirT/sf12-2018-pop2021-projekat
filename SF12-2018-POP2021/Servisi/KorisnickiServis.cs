﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF12_2018_POP2021.Model;
using SF12_2018_POP2021.Model.Korisnik;

namespace SF12_2018_POP2021.Servisi
{
    public class KorisnickiServis : IServis<Korisnik, string>
    {
        public ObservableCollection<Korisnik> Citaj()
        {
            ObservableCollection<Korisnik> korisnici = new ObservableCollection<Korisnik>();
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                String command = @"select * from dbo.Korisnik inner join dbo.Adresa on dbo.Korisnik.Adresa=dbo.Adresa.Id
                                  where dbo.Korisnik.Active=1 and dbo.Adresa.Active=1";
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Korisnik");
                foreach (DataRow row in ds.Tables["Korisnik"].Rows)
                {
                    korisnici.Add(new Korisnik
                    {
                        Ime = (string)row["Ime"],
                        Prezime = (string)row["Prezime"],
                        Jmbg = (string)row["Jmbg"],
                        Email = (string)row["Email"],
                        Lozinka = (string)row["Lozinka"],
                        Pol = (EPol)row["Pol"],
                        TipKorisnika = (ETipKorisnika)row["TipKorisnika"],
                        Adresa = new Adresa
                        {
                            Id = (int)row["Id"],
                            Ulica = (string)row["Ulica"],
                            Broj = (int)row["Broj"],
                            Grad = (string)row["Grad"],
                            Drzava = (string)row["Drzava"],
                            Active = (bool)row["Active"]
                        },
                        Active = (bool)row["Active"]
                    });
                }
            }
            Console.WriteLine("Korisnici " + korisnici.ToList().Count);
            return korisnici;

        }

        public string DodajNovi(Korisnik korisnik)
        {
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Korisnik (Ime, Prezime, Jmbg, Email, Lozinka, Pol, TipKorisnika, Adresa)
										output inserted.jmbg values (@Ime, @Prezime, @Jmbg, @Email, @Lozinka, @Pol, @TipKorisnika, @Adresa)";
                command.Parameters.Add(new SqlParameter("Ime", korisnik.Ime));
                command.Parameters.Add(new SqlParameter("Prezime", korisnik.Prezime));
                command.Parameters.Add(new SqlParameter("Jmbg", korisnik.Jmbg));
                command.Parameters.Add(new SqlParameter("Email", korisnik.Email));
                command.Parameters.Add(new SqlParameter("Lozinka", korisnik.Lozinka));
                command.Parameters.Add(new SqlParameter("Pol", korisnik.Pol));
                command.Parameters.Add(new SqlParameter("TipKorisnika", korisnik.TipKorisnika));
                command.Parameters.Add(new SqlParameter("Adresa", korisnik.Adresa.Id));
                Console.WriteLine("Korisnik sacuvan: " + korisnik.ToString());
                return (string)command.ExecuteScalar();
            }

        }

        public Korisnik NadjiPoId(string id)
        {
            if (id == null) 
            {
                return null;
            }
            Korisnik korisnik = null;
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                string command = @"select * from dbo.Korisnik inner join dbo.Adresa on dbo.Korisnik.Adresa=dbo.Adresa.Id 
                                where Jmbg=@Jmbg and dbo.Korisnik.Active=1 and dbo.Adresa.Active=1";
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                adapter.SelectCommand.Parameters.Add(new SqlParameter("Jmbg", id));
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Korisnik");
                foreach (DataRow row in ds.Tables["Korisnik"].Rows)
                {
                    korisnik = new Korisnik
                    {
                        Ime = (string)row["Ime"],
                        Prezime = (string)row["Prezime"],
                        Jmbg = (string)row["Jmbg"],
                        Email = (string)row["Email"],
                        Lozinka = (string)row["Lozinka"],
                        Pol = (EPol)row["Pol"],
                        TipKorisnika = (ETipKorisnika)row["TipKorisnika"],
                        Adresa = new Adresa
                        {
                            Id = (int)row["Id"],
                            Ulica = (string)row["Ulica"],
                            Broj = (int)row["Broj"],
                            Grad = (string)row["Grad"],
                            Drzava = (string)row["Drzava"],
                            Active = (bool)row["Active"]
                        },
                        Active = (bool)row["Active"]
                    };
                }
            }
            return korisnik;
        }

        public string Azuriraj(Korisnik korisnik)
        {
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.Korisnik set 
                              Ime=@Ime, Prezime=@Prezime, Email=@Email, Lozinka=@Lozinka, Pol=@Pol, TipKorisnika=@TipKorisnika, Adresa=@Adresa
							  output inserted.jmbg where Jmbg=@Jmbg";
                command.Parameters.Add(new SqlParameter("Ime", korisnik.Ime));
                command.Parameters.Add(new SqlParameter("Prezime", korisnik.Prezime));
                command.Parameters.Add(new SqlParameter("Jmbg", korisnik.Jmbg));
                command.Parameters.Add(new SqlParameter("Email", korisnik.Email));
                command.Parameters.Add(new SqlParameter("Lozinka", korisnik.Lozinka));
                command.Parameters.Add(new SqlParameter("Pol", korisnik.Pol));
                command.Parameters.Add(new SqlParameter("TipKorisnika", korisnik.TipKorisnika));
                command.Parameters.Add(new SqlParameter("Adresa", korisnik.Adresa.Id));
                Console.WriteLine("Korisnik azuriran: " + korisnik.ToString());
                return (string)command.ExecuteScalar();
            }
        }

        public string Obrisi(Korisnik korisnik)
        {
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.Korisnik set Active=0 where Jmbg=@Jmbg ";
                command.Parameters.Add(new SqlParameter("Jmbg", korisnik.Jmbg));
                Console.WriteLine("Korisnik obrisan: " + korisnik.ToString());
                command.ExecuteScalar();
                return korisnik.Jmbg;

            }
        }

        public Korisnik Login(string jmbg, string lozinka)
        {
            Korisnik korisnik = NadjiPoId(jmbg);
            if (korisnik != null && string.Equals(korisnik.Jmbg, jmbg) && string.Equals(korisnik.Lozinka, lozinka))
            {
                return korisnik;
            }
            return null;
        }

        public ObservableCollection<Korisnik> Pretrazi(string key, dynamic value, int tipKorisnika)
        {
            ObservableCollection<Korisnik> korisnici = new ObservableCollection<Korisnik>();
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                string command = @"select * from dbo.Korisnik inner join dbo.Adresa on dbo.Korisnik.Adresa=dbo.Adresa.Id 
                                 where dbo.Korisnik.Active=1 and dbo.Adresa.Active=1 and " + key + "=@" + key;
                if (tipKorisnika > -1)
                {
                    command += " and TipKorisnika=@TipKorisnika";
                }
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                adapter.SelectCommand.Parameters.Add(new SqlParameter(key, value));
                if (tipKorisnika > -1)
                {
                    adapter.SelectCommand.Parameters.Add(new SqlParameter("TipKorisnika", tipKorisnika));
                }
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Pretraga");
                foreach (DataRow row in ds.Tables["Pretraga"].Rows)
                {
                    korisnici.Add(new Korisnik
                    {
                        Ime = (string)row["Ime"],
                        Prezime = (string)row["Prezime"],
                        Jmbg = (string)row["Jmbg"],
                        Email = (string)row["Email"],
                        Lozinka = (string)row["Lozinka"],
                        Pol = (EPol)row["Pol"],
                        TipKorisnika = (ETipKorisnika)row["TipKorisnika"],
                        Adresa = new Adresa
                        {
                            Id = (int)row["Id"],
                            Ulica = (string)row["Ulica"],
                            Broj = (int)row["Broj"],
                            Grad = (string)row["Grad"],
                            Drzava = (string)row["Drzava"],
                            Active = (bool)row["Active"]
                        },
                        Active = (bool)row["Active"]
                    });
                }
            }
            return korisnici;
        }

        public ObservableCollection<Korisnik> NadjiRezervisanePolaznike(string jmbg)
        {
            ObservableCollection<Korisnik> korisnici = new ObservableCollection<Korisnik>();
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                string command = @"select * from dbo.Trening 
                                inner join dbo.Korisnik on dbo.Trening.Polaznik=dbo.Korisnik.Jmbg
                                inner join dbo.Adresa on dbo.Korisnik.Adresa=dbo.Adresa.Id 
                                where Instruktor=@Jmbg and Status=1 and TipKorisnika=2 and dbo.Korisnik.Active=1 and dbo.Adresa.Active=1 and dbo.Trening.Active=1";
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                adapter.SelectCommand.Parameters.Add(new SqlParameter("Jmbg", jmbg));
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Pretraga");
                foreach (DataRow row in ds.Tables["Pretraga"].Rows)
                {
                    korisnici.Add(new Korisnik
                    {
                        Ime = (string)row["Ime"],
                        Prezime = (string)row["Prezime"],
                        Jmbg = (string)row["Jmbg"],
                        Email = (string)row["Email"],
                        Lozinka = (string)row["Lozinka"],
                        Pol = (EPol)row["Pol"],
                        TipKorisnika = (ETipKorisnika)row["TipKorisnika"],
                        Adresa = new Adresa
                        {
                            Id = (int)row["Id"],
                            Ulica = (string)row["Ulica"],
                            Broj = (int)row["Broj"],
                            Grad = (string)row["Grad"],
                            Drzava = (string)row["Drzava"],
                            Active = (bool)row["Active"]
                        },
                        Active = (bool)row["Active"]
                    });
                }
            }
            return korisnici;
        }

        public ObservableCollection<Korisnik> PretraziRezervisanePolaznike(string jmbg, string key, dynamic value)
        {
            ObservableCollection<Korisnik> korisnici = new ObservableCollection<Korisnik>();
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                string command = @"select * from dbo.Trening 
                                inner join dbo.Korisnik on dbo.Trening.Polaznik=dbo.Korisnik.Jmbg
                                inner join dbo.Adresa on dbo.Korisnik.Adresa=dbo.Adresa.Id 
                                where Instruktor=@Jmbg and Status=1 and TipKorisnika=2 and dbo.Korisnik.Active=1 and dbo.Adresa.Active=1 and " + key + "=@" + key;
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                adapter.SelectCommand.Parameters.Add(new SqlParameter("Jmbg", jmbg));
                adapter.SelectCommand.Parameters.Add(new SqlParameter(key, value));
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Pretraga");
                foreach (DataRow row in ds.Tables["Pretraga"].Rows)
                {
                    korisnici.Add(new Korisnik
                    {
                        Ime = (string)row["Ime"],
                        Prezime = (string)row["Prezime"],
                        Jmbg = (string)row["Jmbg"],
                        Email = (string)row["Email"],
                        Lozinka = (string)row["Lozinka"],
                        Pol = (EPol)row["Pol"],
                        TipKorisnika = (ETipKorisnika)row["TipKorisnika"],
                        Adresa = new Adresa
                        {
                            Id = (int)row["Id"],
                            Ulica = (string)row["Ulica"],
                            Broj = (int)row["Broj"],
                            Grad = (string)row["Grad"],
                            Drzava = (string)row["Drzava"],
                            Active = (bool)row["Active"]
                        },
                        Active = (bool)row["Active"]
                    });
                }
            }
            return korisnici;
        }

        public ObservableCollection<Korisnik> NadjiPoTipu(ETipKorisnika tipKorisnika)
        {
            return Pretrazi("TipKorisnika", tipKorisnika, -1);
        }

    }
}

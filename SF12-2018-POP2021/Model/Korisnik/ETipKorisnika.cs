﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF12_2018_POP2021.Model.Korisnik
{
    public enum ETipKorisnika
    {
           Administrator,
           Instruktor,
           Polaznik
    }
}

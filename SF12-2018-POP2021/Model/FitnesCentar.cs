﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF12_2018_POP2021.Model
{
    public class FitnesCentar
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _naziv;

        public string Naziv
        {
            get { return _naziv; }
            set { _naziv = value; }
        }

        private Adresa _adresa;


        public Adresa Adresa
        {
            get { return _adresa; }
            set { _adresa = value; }
        }

        private bool _active;
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
        public FitnesCentar()
        {
        }


        public override string ToString()
        {
            return "Naziv: " + Naziv + " Adresa: " + Adresa.ToString();
        }


    }
}

﻿using SF12_2018_POP2021.Model;
using SF12_2018_POP2021.Prozori.Pregled;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SF12_2018_POP2021.Prozori
{
    public abstract class BasePage : Page
    {
        protected OsnovniProzor osnovniProzor;

        protected BasePage()
        {
        }

        protected BasePage(OsnovniProzor prozor)
        {
            osnovniProzor = prozor;
            InicijalizujProzor();
        }

        protected void InicijalizujProzor()
        {
            osnovniProzor.ObrisiMenuItems();
            InicijalizujMenuIteme(osnovniProzor.miFitnes.Items);
            if (Podaci.Instanca.LogovaniKorisnik == null && !(this is LoginProzor) && !(this is PregledKorisnikaProzor)
                || Podaci.Instanca.LogovaniKorisnik != null && !(this is LoginProzor) && !(this is GlavniProzor))
            {
                osnovniProzor.miFitnes.Items.Add(new MenuItem
                {
                    Name = "miNazad",
                    Header = "Nazad"
                });
            }
            osnovniProzor.DodajClickListenerMenuItem();
        }

        protected void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        protected abstract void InicijalizujMenuIteme(ItemCollection menuItemCollection);
        public abstract void menuItem_Click(MenuItem sender, RoutedEventArgs e);
    }
}

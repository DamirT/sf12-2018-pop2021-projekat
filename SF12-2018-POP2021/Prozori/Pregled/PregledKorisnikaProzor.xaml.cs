﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF12_2018_POP2021.Model;
using SF12_2018_POP2021.Model.Korisnik;
using SF12_2018_POP2021.Prozori.DodajIzmeni;

namespace SF12_2018_POP2021.Prozori.Pregled
{
    public partial class PregledKorisnikaProzor : BasePage
    {
        ICollectionView view;
        ETipKorisnika tipKorisnika;

        public PregledKorisnikaProzor(OsnovniProzor osnovniProzor, ETipKorisnika tipKorisnika)
        {
            this.tipKorisnika = tipKorisnika;
            base.osnovniProzor = osnovniProzor;
            base.InicijalizujProzor();
            InitializeComponent();
            lblTip.Content = tipKorisnika;
            cbPretraga.ItemsSource = Podaci.Instanca.VratiPolja();
            if (Podaci.Instanca.LogovaniKorisnik == null || Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator))
            {
                UpdateView(Podaci.Instanca.KorisnickiService.NadjiPoTipu(tipKorisnika));
            }
            else if(Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Instruktor))
            {
                UpdateView(Podaci.Instanca.KorisnickiService.NadjiRezervisanePolaznike(Podaci.Instanca.LogovaniKorisnik.Jmbg));
            }

        }

        private void UpdateView(ObservableCollection<Korisnik> listKorisnika)
        {
            dgKorisnici.ItemsSource = null;
            view = CollectionViewSource.GetDefaultView(listKorisnika);
            dgKorisnici.ItemsSource = view;
            dgKorisnici.IsSynchronizedWithCurrentItem = true;
        }

        protected override void InicijalizujMenuIteme(ItemCollection menuItemCollection)
        {
            if (Podaci.Instanca.LogovaniKorisnik == null)
            {
                menuItemCollection.Add(new MenuItem
                {
                    Name = "miInstruktori",
                    Header = "Instruktori"
                });
                menuItemCollection.Add(new MenuItem
                {
                    Name = "miLogin",
                    Header = "Login"
                });
            }
            else
            {
                if (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator))
                {
                    string tip = "";
                    switch (this.tipKorisnika)
                    {
                        case ETipKorisnika.Administrator:
                            {
                                tip = "Dodaj Administratora";
                                break;
                            }
                        case ETipKorisnika.Instruktor:
                            {
                                tip = "Dodaj Instruktora";
                                break;
                            }
                        case ETipKorisnika.Polaznik:
                            {
                                tip = "Dodaj Polaznika";
                                break;
                            }

                    }
                    menuItemCollection.Add(new MenuItem
                    {
                        Name = "miDodaj",
                        Header = tip
                    }); ;
                }

            }

        }

        public override void menuItem_Click(MenuItem sender, RoutedEventArgs e)
        {
            switch (sender.Name)
            {
                case "miInstruktori":
                    {
                        osnovniProzor.frContentHolder.Content = this;
                        break;
                    }
                case "miLogin":
                    {
                        osnovniProzor.frContentHolder.Content = new LoginProzor(osnovniProzor);
                        break;
                    }
                case "miNazad":
                    {
                        osnovniProzor.frContentHolder.Content = new GlavniProzor(osnovniProzor);
                        break;
                    }
                case "miDodaj":
                    {
                        osnovniProzor.frContentHolder.Content = new KorisnikProzor(osnovniProzor, new Korisnik { TipKorisnika = tipKorisnika }, EStatus.Dodaj);
                        break;
                    }
            }
        }

        private void Row_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var row = ItemsControl.ContainerFromElement((DataGrid)sender,
                                                e.OriginalSource as DependencyObject) as DataGridRow;
            if (row != null)
            {
                //Korisnik selektovan = (Korisnik)view.CurrentItem;
                osnovniProzor.frContentHolder.Content = new KorisnikProzor(osnovniProzor, (Korisnik)row.Item,
                    Podaci.Instanca.LogovaniKorisnik != null && Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator) ? EStatus.IzmeniListu : EStatus.PregledajListu);
            }
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (txtPretraga.Text.Length == 0)
                {
                    if (Podaci.Instanca.LogovaniKorisnik == null || Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator))
                    {
                        UpdateView(Podaci.Instanca.KorisnickiService.NadjiPoTipu(tipKorisnika));
                    }
                    else if (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Instruktor))
                    {
                        UpdateView(Podaci.Instanca.KorisnickiService.NadjiRezervisanePolaznike(Podaci.Instanca.LogovaniKorisnik.Jmbg));
                    }
                }
                else
                {
                    if (Podaci.Instanca.LogovaniKorisnik == null || Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator))
                    {
                        UpdateView(Podaci.Instanca.pretraziKorisnike(cbPretraga.SelectedIndex, txtPretraga.Text, (int)tipKorisnika));
                    }
                    else if (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Instruktor))
                    {
                        UpdateView(Podaci.Instanca.PretraziRezervisanePolaznike(cbPretraga.SelectedIndex, txtPretraga.Text));
                    }
                }
                view.Refresh();
            }
        }

    }
}

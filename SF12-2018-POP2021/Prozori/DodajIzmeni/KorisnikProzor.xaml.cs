﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF12_2018_POP2021.Model;
using SF12_2018_POP2021.Model.Korisnik;
using SF12_2018_POP2021.Prozori.Pregled;

namespace SF12_2018_POP2021.Prozori.DodajIzmeni
{
    public partial class KorisnikProzor : BasePage
    {

        private Korisnik korisnik;
        private Korisnik korisnikKloniran;
        private EStatus eStatus;


        public KorisnikProzor(OsnovniProzor osnovniProzor, Korisnik korisnik, EStatus eStatus = EStatus.Pregledaj) : base(osnovniProzor)
        {
            this.korisnik = korisnik;
            base.DataContext = korisnik;
            this.eStatus = eStatus;
            InitializeComponent();
            Inicijalizuj();
        }

        private void Inicijalizuj()
        {
            cmbTipKorisnika.ItemsSource = Enum.GetValues(typeof(ETipKorisnika)).Cast<ETipKorisnika>();
            cmbPol.ItemsSource = Enum.GetValues(typeof(EPol)).Cast<EPol>();
            PromeniDostuponostKontrolama((eStatus.Equals(EStatus.Dodaj) || eStatus.Equals(EStatus.Registruj)));
            btnIzmeni.Content = (eStatus.Equals(EStatus.Dodaj) || eStatus.Equals(EStatus.Registruj)) ? "Sacuvaj" : "Izmeni";
            btnIzmeni.Visibility =(Podaci.Instanca.LogovaniKorisnik == null && eStatus.Equals(EStatus.Registruj)) || (Podaci.Instanca.LogovaniKorisnik != null
                && (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator) || korisnik.Equals(Podaci.Instanca.LogovaniKorisnik))
                && !eStatus.Equals(EStatus.Pregledaj)) ? Visibility.Visible : Visibility.Hidden;
            btnObrisi.Visibility = Podaci.Instanca.LogovaniKorisnik != null
                && Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator)
                && !Podaci.Instanca.LogovaniKorisnik.Equals(korisnik)
                && (eStatus.Equals(EStatus.Izmeni) || eStatus.Equals(EStatus.IzmeniListu))
                ? Visibility.Visible : Visibility.Hidden;

        }

        private void PromeniDostuponostKontrolama(bool enabled)
        {
            txtIme.IsEnabled = enabled;
            txtPrezime.IsEnabled = enabled;
            txtJmbg.IsEnabled = enabled && (eStatus.Equals(EStatus.Dodaj) || eStatus.Equals(EStatus.Registruj));
            txtEmail.IsEnabled = enabled;
            txtLozinka.IsEnabled = enabled;
            cmbPol.IsEnabled = enabled;
            cmbTipKorisnika.IsEnabled = enabled
                && eStatus.Equals(EStatus.Izmeni)
                && Podaci.Instanca.LogovaniKorisnik != null
                && Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator)
                && !Podaci.Instanca.LogovaniKorisnik.Equals(korisnik);
            txtUlica.IsEnabled = enabled;
            txtBroj.IsEnabled = enabled;
            txtGrad.IsEnabled = enabled;
            txtDrzava.IsEnabled = enabled;
        }

        private void btnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if (string.Equals(btnIzmeni.Content, "Izmeni"))
            {
                korisnikKloniran = korisnik.Clone();
                PromeniDostuponostKontrolama(true);
                btnIzmeni.Content = "Sacuvaj";
            }
            else if (eStatus.Equals(EStatus.Registruj))
            {
                if (korisnik.Jmbg != null && Podaci.Instanca.KorisnickiService.NadjiPoId(korisnik.Jmbg) == null
                    && korisnik.Ime != null
                    && korisnik.Prezime != null
                    && korisnik.Email != null
                    && korisnik.Lozinka != null
                    && korisnik.Adresa.Ulica != null
                    && korisnik.Adresa.Grad != null
                    && korisnik.Adresa.Drzava != null
                    && !korisnik.Ime.Equals("")
                    && !korisnik.Prezime.Equals("")
                    && !korisnik.Jmbg.Equals("")
                    && !korisnik.Email.Equals("")
                    && !korisnik.Lozinka.Equals("")
                    && !korisnik.Adresa.Ulica.Equals("")
                    && !korisnik.Adresa.Broj.Equals("")
                    && !korisnik.Adresa.Grad.Equals("")
                    && !korisnik.Adresa.Drzava.Equals("")
                    )
                { 
                    Podaci.Instanca.DodajKorisnika(korisnik);
                    osnovniProzor.frContentHolder.Content = new LoginProzor(osnovniProzor);
                }
                else
                {
                    MessageBox.Show("Korisnik sa unetim Jmbg-om vec postoji ili neko od polja je prazno.");
                }
            }
            else if (eStatus.Equals(EStatus.Dodaj))
            {
                if (korisnik.Jmbg != null && Podaci.Instanca.KorisnickiService.NadjiPoId(korisnik.Jmbg) == null
                    && korisnik.Ime != null
                    && korisnik.Prezime != null
                    && korisnik.Email != null
                    && korisnik.Lozinka != null
                    && korisnik.Adresa.Ulica != null
                    && korisnik.Adresa.Grad != null
                    && korisnik.Adresa.Drzava != null
                    && !korisnik.Ime.Equals("")
                    && !korisnik.Prezime.Equals("")
                    && !korisnik.Jmbg.Equals("")
                    && !korisnik.Email.Equals("")
                    && !korisnik.Lozinka.Equals("")
                    && !korisnik.Adresa.Ulica.Equals("")
                    && !korisnik.Adresa.Broj.Equals("")
                    && !korisnik.Adresa.Grad.Equals("")
                    && !korisnik.Adresa.Drzava.Equals("")
                    )
                {
                    Podaci.Instanca.DodajKorisnika(korisnik);
                    osnovniProzor.frContentHolder.Content = new PregledKorisnikaProzor(osnovniProzor, korisnik.TipKorisnika);
                }
                else
                {
                    MessageBox.Show("Korisnik sa unetim Jmbg-om vec postoji ili neko od polja je prazno.");
                }
            }
            else
            {
                if (!korisnikKloniran.Equals(korisnik))
                {
                    Podaci.Instanca.AzurirajKorisnika(korisnik);
                }
                PromeniDostuponostKontrolama(false);
                btnIzmeni.Content = "Izmeni";
            }
        }

        private void btnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Podaci.Instanca.ObrisiKorisnika(korisnik);
            osnovniProzor.frContentHolder.Content = new PregledKorisnikaProzor(osnovniProzor, korisnik.TipKorisnika);
        }

        protected override void InicijalizujMenuIteme(ItemCollection menuItemCollection)
        {

        }

        public override void menuItem_Click(MenuItem sender, RoutedEventArgs e)
        {
            switch (sender.Name)
            {
                case "miNazad":
                    {
                        if (eStatus.Equals(EStatus.PregledajListu) || eStatus.Equals(EStatus.IzmeniListu))
                        {
                            osnovniProzor.frContentHolder.Content = new PregledKorisnikaProzor(osnovniProzor, korisnik.TipKorisnika);
                        }
                        else if (Podaci.Instanca.LogovaniKorisnik == null)
                        {
                            osnovniProzor.frContentHolder.Content = new LoginProzor(osnovniProzor);
                        }
                        else
                        {
                            osnovniProzor.frContentHolder.Content = new GlavniProzor(osnovniProzor);
                        }
                        break;
                    }
            }
        }
    }
}

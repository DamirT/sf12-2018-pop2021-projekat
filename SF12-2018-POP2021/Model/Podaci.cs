﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF12_2018_POP2021.Model.Korisnik;
using SF12_2018_POP2021.Servisi;

namespace SF12_2018_POP2021.Model
{
    public sealed class Podaci
    {
        //Ovde kazemo koju bazu koristimo i pravimo string za konekciju koji koristimo u servisu
        public static readonly string CONNECTION_STRING = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private static readonly Podaci instanca = new Podaci();

        private AdresaServis _adresaServis;
        public AdresaServis AdresaService
        {
            get { return _adresaServis; }

        }

        private KorisnickiServis _korisnickiServis;
        public KorisnickiServis KorisnickiService
        {
            get { return _korisnickiServis; }

        }
        private TreningServis _treningServis;
        public TreningServis TreningService
        {
            get { return _treningServis; }

        }
        private FitnesCentarServis _fitnesServis;

        public FitnesCentarServis FitnesCentarService
        {
            get { return _fitnesServis; }

        }

        private Korisnik.Korisnik _logovanKorisnik;

        public Korisnik.Korisnik LogovaniKorisnik
        {
            get { return _logovanKorisnik; }
            set { _logovanKorisnik = value; }
        }

        private Podaci()
        {
            _adresaServis = new AdresaServis();
            _korisnickiServis = new KorisnickiServis();
            _treningServis = new TreningServis();
            _fitnesServis = new FitnesCentarServis();
        }

        static Podaci()
        {

        }

        public static Podaci Instanca
        {
            get { return instanca; }
        }

        public void InicijalizujBazu()
        {
            //Inicijalizuj prvi put bazu
            if (FitnesCentarService.Citaj().Count == 0
                && AdresaService.Citaj().Count == 0
                && KorisnickiService.Citaj().Count == 0)
            {
                Adresa adresa = new Adresa
                {
                    Ulica = "Trg Dositeja Obradovica",
                    Broj = 6,
                    Grad = "Novi Sad",
                    Drzava = "Srbija"
                };
                int id = AdresaService.DodajNovi(adresa);
                adresa.Id = id;

                string jmbg = KorisnickiService.DodajNovi(new Korisnik.Korisnik
                {
                    Ime = "Damir",
                    Prezime = "Tizmonar",
                    Jmbg = "1234567890123",
                    Email = "tiki030@gmail.com",
                    Lozinka = "1234",
                    Pol = EPol.M,
                    TipKorisnika = ETipKorisnika.Administrator,
                    Adresa = adresa
                });

                FitnesCentarService.DodajNovi(new FitnesCentar
                {
                    Naziv = "FTN GYM - Fitnes Centar Fakulteta Tehnickih Nauka",
                    Adresa = adresa
                });
            }

        }


        public string DodajKorisnika(Korisnik.Korisnik korisnik)
        {
            int id = AdresaService.DodajNovi(korisnik.Adresa);
            korisnik.Adresa.Id = id;
            return KorisnickiService.DodajNovi(korisnik);

        }

        public string AzurirajKorisnika(Korisnik.Korisnik korisnik)
        {
            AdresaService.Azuriraj(korisnik.Adresa);
            return KorisnickiService.Azuriraj(korisnik);
        }
        public string ObrisiKorisnika(Korisnik.Korisnik korisnik)
        {
            AdresaService.Obrisi(korisnik.Adresa);
            return KorisnickiService.Obrisi(korisnik);
        }

        public string[] VratiPolja()
        {
            return new string[] { "Ime", "Prezime", "Email", "Ulica", "Broj", "Grad", "Drzava" };
        }

        public ObservableCollection<Korisnik.Korisnik> pretraziKorisnike(int indexPolja, dynamic value, int tipKorisnika)
        {
            if (indexPolja >= 0)
            {
                return KorisnickiService.Pretrazi(VratiPolja()[indexPolja], value, tipKorisnika);

            }
            return null;

        }

        public ObservableCollection<Korisnik.Korisnik> PretraziRezervisanePolaznike(int indexPolja, dynamic value)
        {
            if (indexPolja >= 0)
            {
                return KorisnickiService.PretraziRezervisanePolaznike(LogovaniKorisnik.Jmbg, VratiPolja()[indexPolja], value);

            }
            return null;

        }

        public Korisnik.Korisnik Login(String jmbg, String lozinka)
        {
            LogovaniKorisnik = KorisnickiService.Login(jmbg, lozinka);
            return LogovaniKorisnik;
        }
    }
}

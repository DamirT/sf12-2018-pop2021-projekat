﻿using SF12_2018_POP2021.Model;
using SF12_2018_POP2021.Model.Korisnik;
using SF12_2018_POP2021.Prozori.DodajIzmeni;
using SF12_2018_POP2021.Prozori.Pregled;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SF12_2018_POP2021.Prozori
{
    public partial class LoginProzor : BasePage
    {
        public LoginProzor(OsnovniProzor osnovniProzor) : base(osnovniProzor)
        {
            InitializeComponent();

            //Samo za test
            txtJmbg.Text = "1234567890123";
            txtLozinka.Password = "1234";
        }

        protected override void InicijalizujMenuIteme(ItemCollection menuItemCollection)
        {
            menuItemCollection.Add(new MenuItem
            {
                Name = "miInstruktori",
                Header = "Instruktori"
            });
            menuItemCollection.Add(new MenuItem
            {
                Name = "miLogin",
                Header = "Login"
            });
        }

        public override void menuItem_Click(MenuItem sender, RoutedEventArgs e)
        {
            switch (sender.Name)
            {
                case "miInstruktori":
                    {
                        osnovniProzor.frContentHolder.Content = new PregledKorisnikaProzor(osnovniProzor, ETipKorisnika.Instruktor);
                        break;
                    }
                case "miLogin":
                    {
                        osnovniProzor.frContentHolder.Content = this;
                        break;
                    }
            }
        }

        private void login_Click(object sender, RoutedEventArgs e)
        {
            if (txtJmbg.Text.Length < 13)
            {
                MessageBox.Show("Jmbg mora da sadrzi 13 brojeva.");
                return;
            }

            if (txtLozinka.Password.Length == 0)
            {
                MessageBox.Show("Molim vas unesite Lozinku.");
                return;
            }

            Korisnik korisnik =  Podaci.Instanca.Login(txtJmbg.Text, txtLozinka.Password);
            if (korisnik != null)
            {
                osnovniProzor.frContentHolder.Content = new GlavniProzor(osnovniProzor);
            }
            else
            {
                MessageBox.Show("Unet je pogresan Jmbg ili Lozinka");
            }

        }
        private void register_Click(object sender, RoutedEventArgs e)
        {
            osnovniProzor.frContentHolder.Content = 
                new KorisnikProzor(osnovniProzor, new Korisnik(ETipKorisnika.Polaznik), EStatus.Registruj);
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                login_Click(sender, e);
            }
        }
    }
}

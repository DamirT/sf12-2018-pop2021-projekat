﻿using SF12_2018_POP2021.Model;
using SF12_2018_POP2021.Model.Korisnik;
using SF12_2018_POP2021.Prozori.DodajIzmeni;
using SF12_2018_POP2021.Prozori.Pregled;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SF12_2018_POP2021.Prozori
{
    public partial class GlavniProzor : BasePage
    {

        public GlavniProzor(OsnovniProzor osnovniProzor) : base(osnovniProzor)
        {
            InitializeComponent();
            lblIme.Content = Podaci.Instanca.LogovaniKorisnik.Ime;
            lblTip.Content = Podaci.Instanca.LogovaniKorisnik.TipKorisnika;
        }

        protected override void InicijalizujMenuIteme(ItemCollection menuItemCollection)
        {
            menuItemCollection.Add(new MenuItem
            {
                Name = "miProfil",
                Header = "Moj Profil"
            });

            switch (Podaci.Instanca.LogovaniKorisnik.TipKorisnika)
            {
                case ETipKorisnika.Administrator:
                    {
                        menuItemCollection.Add(new MenuItem
                        {
                            Name = "miAdministratori",
                            Header = "Prikazi Administratore"
                        });
                        menuItemCollection.Add(new MenuItem
                        {
                            Name = "miInstruktori",
                            Header = "Prikazi Instruktore"
                        });
                        menuItemCollection.Add(new MenuItem
                        {
                            Name = "miPolaznici",
                            Header = "Prikazi Polaznike"
                        });
                        menuItemCollection.Add(new MenuItem
                        {
                            Name = "miTreninzi",
                            Header = "Prikazi Treninge"
                        });
                        break;
                    }
                case ETipKorisnika.Instruktor:
                    {
                        menuItemCollection.Add(new MenuItem
                        {
                            Name = "miPolaznici",
                            Header = "Prikazi Polaznike"
                        });
                        menuItemCollection.Add(new MenuItem
                        {
                            Name = "miTreninzi",
                            Header = "Prikazi Treninge"
                        });
                        break;
                    }
                case ETipKorisnika.Polaznik:
                    {
                        menuItemCollection.Add(new MenuItem
                        {
                            Name = "miTreninzi",
                            Header = "Prikazi Treninge"
                        });
                        break;
                    }
            }

            menuItemCollection.Add(new MenuItem
            {
                Name = "miLogout",
                Header = "Logout"
            });
        }

        public override void menuItem_Click(MenuItem sender, RoutedEventArgs e)
        {
            switch (sender.Name)
            {
                case "miProfil":
                    {
                        osnovniProzor.frContentHolder.Content = new KorisnikProzor(osnovniProzor, Podaci.Instanca.LogovaniKorisnik, EStatus.Izmeni);
                        break;
                    }
                case "miAdministratori":
                    {
                        osnovniProzor.frContentHolder.Content = new PregledKorisnikaProzor(osnovniProzor, ETipKorisnika.Administrator);
                        break;
                    }
                case "miInstruktori":
                    {
                        osnovniProzor.frContentHolder.Content = new PregledKorisnikaProzor(osnovniProzor, ETipKorisnika.Instruktor);
                        break;
                    }
                case "miPolaznici":
                    {
                        osnovniProzor.frContentHolder.Content = new PregledKorisnikaProzor(osnovniProzor, ETipKorisnika.Polaznik);
                        break;
                    }
                case "miTreninzi":
                    {
                        osnovniProzor.frContentHolder.Content = new PregledTreningaProzor(osnovniProzor);
                        break;
                    }
                case "miLogout":
                    {
                        Podaci.Instanca.LogovaniKorisnik = null;
                        osnovniProzor.frContentHolder.Content = new LoginProzor(osnovniProzor);
                        break;
                    }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF12_2018_POP2021.Model;

namespace SF12_2018_POP2021.Prozori
{
    public partial class OsnovniProzor : Window
    {

        private RoutedEventHandler menuItemEventHandler;

        public OsnovniProzor()
        {
            InitializeComponent();
            InicijalizujProzor();

        }

        private void InicijalizujProzor()
        {
            menuItemEventHandler = new RoutedEventHandler(menuItem_Click);
            ObrisiMenuItems();
            DodajClickListenerMenuItem();
            // Postavi Login kao prvi prozor
            frContentHolder.Content = new LoginProzor(this);
        }

        public void ObrisiMenuItems()
        {
            for (int i = miFitnes.Items.Count - 1; i > 2; i--)
            {
                miFitnes.Items.RemoveAt(i);
            }
        }

        public void DodajClickListenerMenuItem()
        {
            for (int i = 0; i < miFitnes.Items.Count; i++)
            {
                var item = miFitnes.Items[i];
                if (item is MenuItem)
                {
                    //Obrisi stari lisener
                    ((MenuItem)item).Click -= menuItemEventHandler;
                    //Dodaj novi lisener
                    ((MenuItem)item).Click += menuItemEventHandler;
                }
            }
        }

        private void menuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            switch (menuItem.Name)
            {
                case "miIzadji":
                    {
                        System.Windows.Application.Current.Shutdown();
                        break;
                    }
                case "miOFitnesu":
                    {
                        (new FitnesProzor()).ShowDialog();
                        break;
                    }
                default:
                    {
                        ((BasePage)frContentHolder.Content).menuItem_Click(menuItem, e);

                        break;
                    }

            }
        }


    }
}

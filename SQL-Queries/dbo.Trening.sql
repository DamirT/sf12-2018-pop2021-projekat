﻿CREATE TABLE [dbo].[Trening] (
    [Id]         INT          IDENTITY (1, 1) NOT NULL,
    [Datum]      VARCHAR (20) NOT NULL,
    [Pocetak]    VARCHAR (20) NOT NULL,
    [Trajanje]   VARCHAR (20) NOT NULL,
    [Status]     BIT          NOT NULL,
    [Instruktor] VARCHAR (20) NOT NULL,
    [Polaznik]   VARCHAR (20) NULL,
    [Active]     BIT          DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


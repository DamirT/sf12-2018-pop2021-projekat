﻿CREATE TABLE [dbo].[Korisnik] (
    [Jmbg]         VARCHAR (20) NOT NULL,
    [Ime]          VARCHAR (20) NOT NULL,
    [Prezime]      VARCHAR (20) NOT NULL,
    [Email]        VARCHAR (20) NOT NULL,
    [Lozinka]      VARCHAR (20) NOT NULL,
    [Pol]          INT          NOT NULL,
    [TipKorisnika] INT          NOT NULL,
    [Adresa]       INT          NOT NULL,
    [Active]       BIT          DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Jmbg] ASC)
);


﻿CREATE TABLE [dbo].[Adresa] (
    [Id]     INT          IDENTITY (1, 1) NOT NULL,
    [Ulica]  VARCHAR (40) NOT NULL,
    [Broj]   INT          NOT NULL,
    [Grad]   VARCHAR (20) NOT NULL,
    [Drzava] VARCHAR (20) NOT NULL,
	[Active]     BIT DEFAULT 1 NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


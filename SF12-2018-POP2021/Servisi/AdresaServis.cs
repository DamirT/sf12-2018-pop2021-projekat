﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF12_2018_POP2021.Model;
using SF12_2018_POP2021.Model.Korisnik;

namespace SF12_2018_POP2021.Servisi
{
    public class AdresaServis : IServis<Adresa, int>
    {
        public ObservableCollection<Adresa> Citaj()
        {
            ObservableCollection<Adresa> adrese = new ObservableCollection<Adresa>();
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                String command = @"select * from dbo.Adresa where Active=1";
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Adresa");
                foreach (DataRow row in ds.Tables["Adresa"].Rows)
                {
                    adrese.Add(new Adresa
                    {
                        Id = (int)row["Id"],
                        Ulica = (string)row["Ulica"],
                        Broj = (int)row["Broj"],
                        Grad = (string)row["Grad"],
                        Drzava = (string)row["Drzava"],
                        Active = (bool)row["Active"]
                    });
                }
            }

            Console.WriteLine("Adrese " + adrese.ToList().Count);

            return adrese;

        }

        public int DodajNovi(Adresa adresa)
        {
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Adresa (Ulica, Broj, Grad, Drzava)
										output inserted.id values (@Ulica, @Broj, @Grad, @Drzava)";
                command.Parameters.Add(new SqlParameter("Ulica", adresa.Ulica));
                command.Parameters.Add(new SqlParameter("Broj", adresa.Broj));
                command.Parameters.Add(new SqlParameter("Grad", adresa.Grad));
                command.Parameters.Add(new SqlParameter("Drzava", adresa.Drzava));
                Console.WriteLine("Adresa sacuvana: " + adresa.ToString());
                return (int)command.ExecuteScalar();
            }
        }

        public Adresa NadjiPoId(int id)
        {
            Adresa adresa = null;
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                string command = @"select * from dbo.Adresa where Id=@Id and Active=1 ";
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                adapter.SelectCommand.Parameters.Add(new SqlParameter("Id", id));
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Adresa");
                foreach (DataRow row in ds.Tables["Adresa"].Rows)
                {
                    adresa = new Adresa
                    {
                        Id = (int)row["Id"],
                        Ulica = (string)row["Ulica"],
                        Broj = (int)row["Broj"],
                        Grad = (string)row["Grad"],
                        Drzava = (string)row["Drzava"],
                        Active = (bool)row["Active"]
                    };
                }
            }
            return adresa;
        }

        public int Azuriraj(Adresa adresa)
        {
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.Adresa set Ulica=@Ulica, Broj=@Broj, Grad=@Grad, Drzava=@Drzava
										output inserted.id where Id=@Id";
                command.Parameters.Add(new SqlParameter("Id", adresa.Id));
                command.Parameters.Add(new SqlParameter("Ulica", adresa.Ulica));
                command.Parameters.Add(new SqlParameter("Broj", adresa.Broj));
                command.Parameters.Add(new SqlParameter("Grad", adresa.Grad));
                command.Parameters.Add(new SqlParameter("Drzava", adresa.Drzava));
                Console.WriteLine("Adresa azurirana: " + adresa.ToString());
                return (int)command.ExecuteScalar();
            }
        }

        public int Obrisi(Adresa adresa)
        {
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.Adresa set Active=0 where Id=@Id ";
                command.Parameters.Add(new SqlParameter("Id", adresa.Id));
                Console.WriteLine("Adresa obrisana: " + adresa.ToString());
                command.ExecuteScalar();
                return adresa.Id;
            }
        }

        public ObservableCollection<Adresa> Pretrazi(string key, dynamic value)
        {
            ObservableCollection<Adresa> adrese = new ObservableCollection<Adresa>();
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                string command = @"select * from dbo.Adresa where Active=1 and " + key + "=@" + key;
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                adapter.SelectCommand.Parameters.Add(new SqlParameter(key, value));
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Adresa");
                foreach (DataRow row in ds.Tables["Adresa"].Rows)
                {
                    adrese.Add(new Adresa
                    {
                        Id = (int)row["Id"],
                        Ulica = (string)row["Ulica"],
                        Broj = (int)row["Broj"],
                        Grad = (string)row["Grad"],
                        Drzava = (string)row["Drzava"]
                    });
                }
            }
            return adrese;
        }
    }
}

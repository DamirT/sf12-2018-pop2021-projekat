﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF12_2018_POP2021.Model.Korisnik
{
    public class Korisnik
    {
        private string _ime;

        public string Ime
        {
            get { return _ime; }
            set { _ime = value; }
        }

        private string _prezime;

        public string Prezime
        {
            get { return _prezime; }
            set { _prezime = value; }
        }

        private string _jmbg;

        public string Jmbg
        {
            get { return _jmbg; }
            set { _jmbg = value; }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _lozinka;

        public string Lozinka
        {
            get { return _lozinka; }
            set { _lozinka = value; }
        }

        private EPol _pol;

        public EPol Pol
        {
            get { return _pol; }
            set { _pol = value; }
        }

        protected ETipKorisnika _tipKorisnika;

        public ETipKorisnika TipKorisnika
        {
            get { return _tipKorisnika; }
            set { _tipKorisnika = value; }
        }

        private Adresa _adresa;

        public Adresa Adresa
        {
            get { return _adresa; }
            set { _adresa = value; }
        }

        private bool _active;
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public Korisnik()
        {
            Adresa = new Adresa();
        }

        public Korisnik(ETipKorisnika tipKorisnika)
        {
            Adresa = new Adresa();
            TipKorisnika = tipKorisnika;
        }

        public Korisnik Clone()
        {
            return new Korisnik
            {
                Ime = this.Ime,
                Prezime = this.Prezime,
                Jmbg = this.Jmbg,
                Email = this.Email,
                Lozinka = this.Lozinka,
                Pol = this.Pol,
                TipKorisnika = this.TipKorisnika,
                Adresa = this.Adresa.Clone(),
                Active = this.Active
            };
        }


        public override string ToString()
        {
            return Ime + " " + Prezime;
        }

        public override bool Equals(object obj)
        {
            if (obj is Korisnik)
            {
                return string.Equals(((Korisnik)obj).Ime, Ime)
                    && string.Equals(((Korisnik)obj).Prezime, Prezime)
                    && string.Equals(((Korisnik)obj).Jmbg, Jmbg)
                    && string.Equals(((Korisnik)obj).Email, Email)
                    && string.Equals(((Korisnik)obj).Lozinka, Lozinka)
                    && ((Korisnik)obj).Pol.Equals(Pol)
                    && ((Korisnik)obj).TipKorisnika.Equals(TipKorisnika)
                    && ((Korisnik)obj).Adresa.Equals(Adresa);

            }
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF12_2018_POP2021.Model
{
    public class Trening
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _datum;

        public string Datum
        {
            get { return _datum; }
            set { _datum = value; }
        }

        private string _pocetak;

        public string Pocetak
        {
            get { return _pocetak; }
            set { _pocetak = value; }
        }

        private string _trajanje;

        public string Trajanje
        {
            get { return _trajanje; }
            set { _trajanje = value; }
        }

        private bool _status;

        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private Korisnik.Korisnik _instruktor;

        public Korisnik.Korisnik Instruktor
        {
            get { return _instruktor; }
            set { _instruktor = value; }
        }

        private Korisnik.Korisnik _polaznik;

        public Korisnik.Korisnik Polaznik
        {
            get { return _polaznik; }
            set { _polaznik = value; }
        }

        private bool _active;
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
        
        public Trening()
        {
        }

        public Trening Clone()
        {
            return new Trening
            {
                Id = this.Id,
                Datum = this.Datum,
                Pocetak = this.Pocetak,
                Trajanje = this.Trajanje,
                Status = this.Status,
                Instruktor = this.Instruktor.Clone(),
                Polaznik = this.Polaznik.Clone(),
                Active = this.Active
            };
        }

        public override string ToString()
        {
            return "Datum: " + Datum + " Pocetak: " + Pocetak + " Trajanje: " + Trajanje + " Status: " + Status + " Active: " + Active;
        }
    }
}

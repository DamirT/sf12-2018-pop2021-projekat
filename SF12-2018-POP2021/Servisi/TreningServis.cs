﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF12_2018_POP2021.Model;
using SF12_2018_POP2021.Model.Korisnik;

namespace SF12_2018_POP2021.Servisi
{
    public class TreningServis : IServis<Trening, int>
    {
        public ObservableCollection<Trening> Citaj()
        {
            ObservableCollection<Trening> treninzi = new ObservableCollection<Trening>();
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                String command = @"select * from dbo.Trening where Active=1";
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Trening");
                foreach (DataRow row in ds.Tables["Trening"].Rows)
                {
                    treninzi.Add(new Trening
                    {
                        Id = (int)row["Id"],
                        Datum = (string)row["Datum"],
                        Pocetak = (string)row["Pocetak"],
                        Trajanje = (string)row["Trajanje"],
                        Status = (bool)row["Status"],
                        Instruktor = Podaci.Instanca.KorisnickiService.NadjiPoId((string)row["Instruktor"]),
                        Polaznik = row["Polaznik"] != DBNull.Value ? Podaci.Instanca.KorisnickiService.NadjiPoId((string)row["Polaznik"]) : null,
                        Active = (bool)row["Active"]
                    });
                }
            }

            Console.WriteLine("Treninzi " + treninzi.ToList().Count);
            return treninzi;

        }

        public int DodajNovi(Trening trening)
        {
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Trening (Datum, Pocetak, Trajanje, Status, Instruktor";
                if (trening.Polaznik == null)
                {
                    command.CommandText += ") output inserted.id values(@Datum, @Pocetak, @Trajanje, @Status, @Instruktor)";
                }
                else
                {
                    command.CommandText += ", Polaznik) output inserted.id values(@Datum, @Pocetak, @Trajanje, @Status, @Instruktor, @Polaznik)";

                }
                command.Parameters.Add(new SqlParameter("Datum", trening.Datum));
                command.Parameters.Add(new SqlParameter("Pocetak", trening.Pocetak));
                command.Parameters.Add(new SqlParameter("Trajanje", trening.Trajanje));
                command.Parameters.Add(new SqlParameter("Status", trening.Status));
                command.Parameters.Add(new SqlParameter("Instruktor", trening.Instruktor.Jmbg));
                if (trening.Polaznik != null)
                {
                    command.Parameters.Add(new SqlParameter("Polaznik", trening.Polaznik.Jmbg));
                }
                Console.WriteLine("Trening sacuvan: " + trening.ToString());
                return (int)command.ExecuteScalar();
            }

        }

        public Trening NadjiPoId(int id)
        {
            Trening trening = null;
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                string command = @"select * from dbo.Trening where Id=@Id and Active=1";
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                adapter.SelectCommand.Parameters.Add(new SqlParameter("Id", id));
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Trening");
                foreach (DataRow row in ds.Tables["Trening"].Rows)
                {
                    trening = new Trening
                    {
                        Id = (int)row["Id"],
                        Datum = (string)row["Datum"],
                        Pocetak = (string)row["Pocetak"],
                        Trajanje = (string)row["Trajanje"],
                        Status = (bool)row["Status"],
                        Instruktor = Podaci.Instanca.KorisnickiService.NadjiPoId((string)row["Instruktor"]),
                        Polaznik = row["Polaznik"] != DBNull.Value ? Podaci.Instanca.KorisnickiService.NadjiPoId((string)row["Polaznik"]) : null,
                        Active = (bool)row["Active"]
                    };
                }
            }
            return trening;
        }

        public int Azuriraj(Trening trening)
        {
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.Trening set 
                                        Datum=@Datum, Pocetak=@Pocetak, Trajanje=@Trajanje, Status=@Status, Instruktor=@Instruktor";
                if (trening.Polaznik == null)
                {
                    command.CommandText += " output inserted.id where Id=@Id";
                }
                else
                {
                    command.CommandText += ", Polaznik=@Polaznik output inserted.id where Id=@Id";

                }
                command.Parameters.Add(new SqlParameter("Id", trening.Id));
                command.Parameters.Add(new SqlParameter("Datum", trening.Datum));
                command.Parameters.Add(new SqlParameter("Pocetak", trening.Pocetak));
                command.Parameters.Add(new SqlParameter("Trajanje", trening.Trajanje));
                command.Parameters.Add(new SqlParameter("Status", trening.Status));
                command.Parameters.Add(new SqlParameter("Instruktor", trening.Instruktor.Jmbg));
                if (trening.Polaznik != null)
                {
                    command.Parameters.Add(new SqlParameter("Polaznik", trening.Polaznik.Jmbg));
                }
                Console.WriteLine("Trening azuriran: " + trening.ToString());
                return (int)command.ExecuteScalar();

            }
        }

        public int Obrisi(Trening trening)
        {
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.Trening set Active=0 where Id=@Id ";
                command.Parameters.Add(new SqlParameter("Id", trening.Id));
                Console.WriteLine("Trening obrisan: " + trening.ToString());
                command.ExecuteScalar();
                return trening.Id;
            }
        }

        public ObservableCollection<Trening> NadjiRezervisaneTreninge(string jmbg)
        {
            ObservableCollection<Trening> treninzi = new ObservableCollection<Trening>();
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                string command = @"select * from dbo.Trening where Instruktor=@Jmbg and Status=1 and Active=1";
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                adapter.SelectCommand.Parameters.Add(new SqlParameter("Jmbg", jmbg));
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Trening");
                foreach (DataRow row in ds.Tables["Trening"].Rows)
                {
                    treninzi.Add(new Trening
                    {
                        Id = (int)row["Id"],
                        Datum = (string)row["Datum"],
                        Pocetak = (string)row["Pocetak"],
                        Trajanje = (string)row["Trajanje"],
                        Status = (bool)row["Status"],
                        Instruktor = Podaci.Instanca.KorisnickiService.NadjiPoId((string)row["Instruktor"]),
                        Polaznik = row["Polaznik"] != DBNull.Value ? Podaci.Instanca.KorisnickiService.NadjiPoId((string)row["Polaznik"]) : null,
                        Active = (bool)row["Active"]
                    });
                }
            }
            return treninzi;
        }

        public ObservableCollection<Trening> NadjiTreningePoDatumu(string jmbg, string datum)
        {
            ObservableCollection<Trening> treninzi = new ObservableCollection<Trening>();
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                string command = @"select * from dbo.Trening where Instruktor=@Jmbg and Datum=@Datum and Active=1";
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                adapter.SelectCommand.Parameters.Add(new SqlParameter("Jmbg", jmbg));
                adapter.SelectCommand.Parameters.Add(new SqlParameter("Datum", datum));
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Trening");
                foreach (DataRow row in ds.Tables["Trening"].Rows)
                {
                    treninzi.Add(new Trening
                    {
                        Id = (int)row["Id"],
                        Datum = (string)row["Datum"],
                        Pocetak = (string)row["Pocetak"],
                        Trajanje = (string)row["Trajanje"],
                        Status = (bool)row["Status"],
                        Instruktor = Podaci.Instanca.KorisnickiService.NadjiPoId((string)row["Instruktor"]),
                        Polaznik = row["Polaznik"] != DBNull.Value ? Podaci.Instanca.KorisnickiService.NadjiPoId((string)row["Polaznik"]) : null,
                        Active = (bool)row["Active"]
                    });
                }
            }
            return treninzi;
        }

        public ObservableCollection<Trening> NadjiPoKorisniku(Korisnik korisnik)
        {
            ObservableCollection<Trening> treninzi = new ObservableCollection<Trening>();
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                string command = @"select * from dbo.Trening where Active=1 and " + korisnik.TipKorisnika.ToString() + "=@Jmbg";
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                adapter.SelectCommand.Parameters.Add(new SqlParameter("Jmbg", korisnik.Jmbg));
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Trening");
                foreach (DataRow row in ds.Tables["Trening"].Rows)
                {
                    treninzi.Add(new Trening
                    {
                        Id = (int)row["Id"],
                        Datum = (string)row["Datum"],
                        Pocetak = (string)row["Pocetak"],
                        Trajanje = (string)row["Trajanje"],
                        Status = (bool)row["Status"],
                        Instruktor = Podaci.Instanca.KorisnickiService.NadjiPoId((string)row["Instruktor"]),
                        Polaznik = row["Polaznik"] != DBNull.Value ? Podaci.Instanca.KorisnickiService.NadjiPoId((string)row["Polaznik"]) : null,
                        Active = (bool)row["Active"]
                    });
                }
            }
            return treninzi;
        }
    }
}

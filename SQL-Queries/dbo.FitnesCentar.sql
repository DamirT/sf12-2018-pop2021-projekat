﻿CREATE TABLE [dbo].[FitnesCentar] (
    [Id]     INT          IDENTITY (1, 1) NOT NULL,
    [Naziv]  VARCHAR (80) NOT NULL,
    [Adresa] INT          NOT NULL,
    [Active] BIT          DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


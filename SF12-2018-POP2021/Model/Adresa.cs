﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF12_2018_POP2021.Model
{
    public class Adresa
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _ulica;

        public string Ulica
        {
            get { return _ulica; }
            set { _ulica = value; }
        }

        private int _broj;

        public int Broj
        {
            get { return _broj; }
            set { _broj = value; }
        }

        private string _grad;

        public string Grad
        {
            get { return _grad; }
            set { _grad = value; }
        }

        private string _drzava;

        public string Drzava
        {
            get { return _drzava; }
            set { _drzava = value; }
        }

        private bool _active;
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public Adresa()
        {

        }

        public Adresa Clone()
        {
            return new Adresa
            {
                Id = this.Id,
                Ulica = this.Ulica,
                Broj = this.Broj,
                Grad = this.Grad,
                Drzava = this.Drzava,
                Active = this.Active
            };
        }

        public override bool Equals(object obj)
        {
            if (obj is Adresa)
            {
                return ((Adresa)obj).Id == Id
                    && string.Equals(((Adresa)obj).Ulica, Ulica)
                    && ((Adresa)obj).Broj == Broj
                    && string.Equals(((Adresa)obj).Grad, Grad)
                    && string.Equals(((Adresa)obj).Drzava, Drzava);
            }
            return false;
        }

        public override string ToString()
        {
            return "Ulica: " + Ulica + ", Broj: " + Broj + ", Grad: " + Grad + ", Drzava: " + Drzava + ", Active: " + Active;
        }

    }
}

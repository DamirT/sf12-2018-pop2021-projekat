﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF12_2018_POP2021.Model;
using SF12_2018_POP2021.Model.Korisnik;
using SF12_2018_POP2021.Prozori.Pregled;

namespace SF12_2018_POP2021.Prozori.DodajIzmeni
{
    public partial class TreningProzor : BasePage
    {

        private Trening trening;
        private Trening treningKloniran;
        private EStatus eStatus;


        public TreningProzor(OsnovniProzor osnovniProzor, Trening trening, EStatus eStatus = EStatus.Pregledaj) : base(osnovniProzor)
        {
            this.trening = trening;
            base.DataContext = trening;
            this.eStatus = eStatus;
            InitializeComponent();
            Inicijalizuj();
        }

        private void Inicijalizuj()
        {
            PromeniDostuponostKontrolama(eStatus.Equals(EStatus.Dodaj));
            btnIzmeni.Content = eStatus.Equals(EStatus.Dodaj) ? "Sacuvaj" : "Izmeni";
            btnIzmeni.Visibility = (Podaci.Instanca.LogovaniKorisnik != null
                && Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator)
                && !eStatus.Equals(EStatus.Pregledaj)) || eStatus.Equals(EStatus.Dodaj) 
                || (eStatus.Equals(EStatus.IzmeniListu) && !trening.Status) ? Visibility.Visible : Visibility.Hidden;
            btnObrisi.Visibility = Podaci.Instanca.LogovaniKorisnik != null
                && (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator)
                || (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Instruktor) && !trening.Status)
                || (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Polaznik) && trening.Status && Podaci.Instanca.TreningService.NadjiPoId(trening.Id).Polaznik.Equals(Podaci.Instanca.LogovaniKorisnik)))
                && eStatus.Equals(EStatus.IzmeniListu)
                ? Visibility.Visible : Visibility.Hidden;

        }

        private void PromeniDostuponostKontrolama(bool enabled)
        {
            txtDatum.IsEnabled = enabled;
            txtPocetak.IsEnabled = enabled;
            txtStatus.IsEnabled = enabled && !eStatus.Equals(EStatus.Dodaj);
            txtTrajanje.IsEnabled = enabled;
        }

        private void btnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if (string.Equals(btnIzmeni.Content, "Izmeni"))
            {
                if (trening.Polaznik != null)
                {
                    treningKloniran = trening.Clone();
                }
                if (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Polaznik))
                {
                    txtStatus.IsEnabled = true;
                }
                else if (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Instruktor))
                {
                    PromeniDostuponostKontrolama(true);
                    txtStatus.IsEnabled = false;
                }
                else
                {
                    PromeniDostuponostKontrolama(true);
                }
                btnIzmeni.Content = "Sacuvaj";
            }
            else if (eStatus.Equals(EStatus.Dodaj))
            {
                if (trening.Datum != null
                    && trening.Datum != null
                    && trening.Pocetak != null
                    && trening.Trajanje != null
                    && !trening.Datum.Equals("")
                    && !trening.Pocetak.Equals("")
                    && !trening.Trajanje.Equals("")
                    && !trening.Status.Equals("")
                    )
                {
                    Podaci.Instanca.TreningService.DodajNovi(trening);
                    osnovniProzor.frContentHolder.Content = new PregledTreningaProzor(osnovniProzor);
                }
                else
                {
                    MessageBox.Show("Neko od polja je prazno.");
                }
            }
            else
            {
                if (treningKloniran==null || !treningKloniran.Equals(trening))
                {
                    if (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Polaznik))
                    {
                        trening.Polaznik = Podaci.Instanca.LogovaniKorisnik;
                    }
                    Podaci.Instanca.TreningService.Azuriraj(trening);
                }
                PromeniDostuponostKontrolama(false);
                btnIzmeni.Content = "Izmeni";
            }
        }

        private void btnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Podaci.Instanca.TreningService.Obrisi(trening);
            osnovniProzor.frContentHolder.Content = new PregledTreningaProzor(osnovniProzor);
        }

        protected override void InicijalizujMenuIteme(ItemCollection menuItemCollection)
        {

        }

        public override void menuItem_Click(MenuItem sender, RoutedEventArgs e)
        {
            switch (sender.Name)
            {
                case "miNazad":
                    {
                        osnovniProzor.frContentHolder.Content = new PregledTreningaProzor(osnovniProzor);
                        break;
                    }
            }
        }
    }
}

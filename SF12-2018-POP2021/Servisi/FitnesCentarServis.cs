﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF12_2018_POP2021.Model;
using SF12_2018_POP2021.Model.Korisnik;

namespace SF12_2018_POP2021.Servisi
{
    public class FitnesCentarServis : IServis<FitnesCentar, int>
    {
        public ObservableCollection<FitnesCentar> Citaj()
        {
            ObservableCollection<FitnesCentar> fitnesCentri = new ObservableCollection<FitnesCentar>();
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                String command = @"select * from dbo.FitnesCentar where Active=1";
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                DataSet ds = new DataSet();
                adapter.Fill(ds, "FitnesCentar");
                foreach (DataRow row in ds.Tables["FitnesCentar"].Rows)
                {
                    fitnesCentri.Add(new FitnesCentar
                    {
                        Id = (int)row["Id"],
                        Naziv = (string)row["Naziv"],
                        Adresa = Podaci.Instanca.AdresaService.NadjiPoId((int)row["Adresa"]),
                        Active = (bool)row["Active"]
                    });
                }
            }

            Console.WriteLine("FitnesCentri " + fitnesCentri.ToList().Count);
            return fitnesCentri;

        }

        public int DodajNovi(FitnesCentar fitnesCentar)
        {
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.FitnesCentar (Naziv, Adresa)
										output inserted.id values (@Naziv, @Adresa)";
                command.Parameters.Add(new SqlParameter("Naziv", fitnesCentar.Naziv));
                command.Parameters.Add(new SqlParameter("Adresa", fitnesCentar.Adresa.Id));
                Console.WriteLine("FitnesCentar sacuvan: " + fitnesCentar.ToString());
                return (int)command.ExecuteScalar();
            }

        }

        public FitnesCentar NadjiPoId(int id)
        {
            FitnesCentar fitnesCentar = null;
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                string command = @"select * from dbo.FitnesCentar where Id=@Id and Active=1";
                SqlDataAdapter adapter = new SqlDataAdapter(command, conn);
                adapter.SelectCommand.Parameters.Add(new SqlParameter("Id", id));
                DataSet ds = new DataSet();
                adapter.Fill(ds, "FitnesCentar");
                foreach (DataRow row in ds.Tables["FitnesCentar"].Rows)
                {
                    fitnesCentar = new FitnesCentar
                    {
                        Id = (int)row["Id"],
                        Naziv = (string)row["Naziv"],
                        Adresa = Podaci.Instanca.AdresaService.NadjiPoId((int)row["Adresa"]),
                        Active = (bool)row["Active"]
                    };
                }
            }
            return fitnesCentar;
        }

        public int Azuriraj(FitnesCentar fitnesCentar)
        {
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.FitnesCentar set Naziv=@Naziv, Adresa=@Adresa
										output inserted.id where Id=@Id";
                command.Parameters.Add(new SqlParameter("Id", fitnesCentar.Id));
                command.Parameters.Add(new SqlParameter("Naziv", fitnesCentar.Naziv));
                command.Parameters.Add(new SqlParameter("Adresa", fitnesCentar.Adresa.Id));
                Console.WriteLine("FitnesCentar azuriran: " + fitnesCentar.ToString());
                return (int)command.ExecuteScalar();
            }
        }

        public int Obrisi(FitnesCentar fitnesCentar)
        {
            using (SqlConnection conn = new SqlConnection(Podaci.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.FitnesCentar set Active=0 where Id=@Id ";
                command.Parameters.Add(new SqlParameter("Id", fitnesCentar.Id));
                Console.WriteLine("FitnesCentar obrisan: " + fitnesCentar.ToString());
                command.ExecuteScalar();
                return fitnesCentar.Id;
            }
        }
    }
}

﻿using SF12_2018_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SF12_2018_POP2021.Prozori
{
    public partial class FitnesProzor : Window
    {
        public FitnesProzor()
        {
            InitializeComponent();
            ObservableCollection<FitnesCentar> fitnesCentar = Podaci.Instanca.FitnesCentarService.Citaj();
            lblNaziv.Content = fitnesCentar.ElementAt(0).Naziv;
            lblAdresa.Content = fitnesCentar.ElementAt(0).Adresa.ToString();
        }

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF12_2018_POP2021.Model;
using SF12_2018_POP2021.Model.Korisnik;
using SF12_2018_POP2021.Prozori.DodajIzmeni;

namespace SF12_2018_POP2021.Prozori.Pregled
{
    public partial class PregledTreningaProzor : BasePage
    {
        ICollectionView view;

        public PregledTreningaProzor(OsnovniProzor osnovniProzor) : base(osnovniProzor)
        {
            base.InicijalizujProzor();
            InitializeComponent();
            if (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Instruktor))
            {
                UpdateView(Podaci.Instanca.TreningService.NadjiPoKorisniku(Podaci.Instanca.LogovaniKorisnik));
                lblInstruktori.Visibility = Visibility.Hidden;
                cbInstruktori.Visibility = Visibility.Hidden;
                lblDatum.Visibility = Visibility.Visible;
                dpDatum.Visibility = Visibility.Visible;
            }
            else if (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Polaznik))
            {
                lblInstruktori.Visibility = Visibility.Visible;
                cbInstruktori.Visibility = Visibility.Visible;
                lblDatum.Visibility = Visibility.Hidden;
                dpDatum.Visibility = Visibility.Hidden;
                ObservableCollection<Korisnik> korisnici = Podaci.Instanca.KorisnickiService.NadjiPoTipu(ETipKorisnika.Instruktor);
                cbInstruktori.ItemsSource = korisnici;
                UpdateView(Podaci.Instanca.TreningService.NadjiPoKorisniku((Korisnik)cbInstruktori.SelectedItem));
            }
            else
            {
                lblInstruktori.Visibility = Visibility.Visible;
                cbInstruktori.Visibility = Visibility.Visible;
                ObservableCollection<Korisnik> korisnici = Podaci.Instanca.KorisnickiService.NadjiPoTipu(ETipKorisnika.Instruktor);
                cbInstruktori.ItemsSource = korisnici;
                UpdateView(Podaci.Instanca.TreningService.Citaj());

            }

        }

        private void UpdateView(ObservableCollection<Trening> listTreninga)
        {
            dgKorisnici.ItemsSource = null;
            view = CollectionViewSource.GetDefaultView(listTreninga);
            dgKorisnici.ItemsSource = view;
            dgKorisnici.IsSynchronizedWithCurrentItem = true;
        }

        protected override void InicijalizujMenuIteme(ItemCollection menuItemCollection)
        {
            if (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator)
                || Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Instruktor))
            {
                menuItemCollection.Add(new MenuItem
                {
                    Name = "miDodaj",
                    Header = "Dodaj Trening"
                }); ;
            }


        }

        public override void menuItem_Click(MenuItem sender, RoutedEventArgs e)
        {
            switch (sender.Name)
            {
                case "miNazad":
                    {
                        osnovniProzor.frContentHolder.Content = new GlavniProzor(osnovniProzor);
                        break;
                    }
                case "miDodaj":
                    {
                        Trening trening = new Trening();
                        if (Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator))
                        {
                            trening.Instruktor = (Korisnik)cbInstruktori.SelectedItem;
                        }
                        else
                        {
                            trening.Instruktor = Podaci.Instanca.LogovaniKorisnik;
                        }
                        
                        osnovniProzor.frContentHolder.Content = new TreningProzor(osnovniProzor, trening, EStatus.Dodaj);
                        break;
                    }
            }
        }

        private void dpick_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dpDatum.SelectedDate != null)
            {
                UpdateView(Podaci.Instanca.TreningService.NadjiTreningePoDatumu(Podaci.Instanca.LogovaniKorisnik.Jmbg, dpDatum.SelectedDate.Value.ToShortDateString()));
            }
            else
            {
                UpdateView(Podaci.Instanca.TreningService.NadjiPoKorisniku(Podaci.Instanca.LogovaniKorisnik));
            }

        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!Podaci.Instanca.LogovaniKorisnik.TipKorisnika.Equals(ETipKorisnika.Administrator)) {
                UpdateView(Podaci.Instanca.TreningService.NadjiPoKorisniku((Korisnik)cbInstruktori.SelectedItem));
            }
        }

        private void Row_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var row = ItemsControl.ContainerFromElement((DataGrid)sender,
                e.OriginalSource as DependencyObject) as DataGridRow;
            if (row != null)
            {
                osnovniProzor.frContentHolder.Content = osnovniProzor.frContentHolder.Content = new TreningProzor(osnovniProzor, (Trening)row.Item, EStatus.IzmeniListu);
            }
        }

    }
}
